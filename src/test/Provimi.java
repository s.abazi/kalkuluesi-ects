/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

class Provimi
        
        
{
	private final int provimi;
        private final int deadline;
        private final int ect;

	public Provimi(int provimi, int deadline, int ect) {
		this.provimi = provimi;
		this.deadline = deadline;
		this.ect = ect;
	}

	public int getDeadline() {
		return deadline;
	}

	public int getEct() {
		return ect;
	}

	public int getProvimi() {
		return provimi;
	}
};
