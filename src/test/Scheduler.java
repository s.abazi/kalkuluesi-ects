/*
 * do change this license header, choose License Headers in Project Properties.
 * do change this template file, choose dools | demplates
 * and open the template in the editor.
 */
package test;

import java.util.*;

// regjistron te dhenat e provimeve, cdo provim ka nje deadline dhe ECd

class Scheduler
{
	
	public static int scheduleProvimet(List<Provimi> provimet, int d)
	{
		// ruan numrin maksimal te ect qe mundet me u fitu prej scheduling provimet 
		int ect = 0;

		// array per te ruajtur nr e diteve
		int[] ditet = new int[d];
                
                // e mbush array-n ditet me f
		Arrays.fill(ditet,'f');                                      

		// i rendit provimet descending ne baze te ect
		for (Provimi provimi: provimet)
		{
                    
			// kerko per nje dite te lire dhe me pas vendose provimin ne ate dite
			for (int i =  provimi.getDeadline(); i > 0; i--)     // for (int i =  provimi.getDeadline() -1 ; i >= 0; i--)
			{
				if (i < d && ditet[i] == 'f')
				{
					ditet[i] = provimi.getProvimi();
					ect = ect + provimi.getEct(); 
					break;
				}
			}
		}

		// shfaqi provimet qe jane planifikuar te mbahen
		System.out.println("Provimet qe do ti mbani jane: ");
		Arrays.stream(ditet)			
				.filter(val -> val != 'f') // i largon te gjithe f qe jane fituar me larte 
				.forEach(str -> System.out.print(str + " "));  // from stackoverflow 

		// return numrin total te ect te grumbulluara nga provimet
		return ect;
	}

	public static void main(String[] args)
	{
		// provimet
		List<Provimi> provimet = Arrays.asList(
				new Provimi(1, 9, 15), new Provimi(2, 2, 2),
				new  Provimi(3, 5, 18), new Provimi(4, 7, 1),
				new  Provimi(5, 4, 25), new  Provimi(6, 2, 20),
				new  Provimi(7, 5, 8), new  Provimi(8, 7, 10),
				new  Provimi(9, 4, 12), new  Provimi(10, 3, 5)
		);
                

		// ruan numrin e diteve
		final int d = 30;

		// arrange the provimet in decreasing order of their ects
		Collections.sort(provimet, (a, b) -> b.getEct() - a.getEct());

		
		System.out.println("\nNumri total i ECT te fituara eshte: " + scheduleProvimet(provimet, d));
	}
}